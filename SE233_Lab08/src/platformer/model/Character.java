package platformer.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.view.Platform;


public class Character extends Pane {
    public static final int CHARACTER_WIDTH = 64;
    public static final int CHARACTER_HEIGHT = 64;
    private Image characterImg;
    private ImageView imageView;
    private int x;
    private int y;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    int xVelocity = 5;
    int yVelocity = 3;
    boolean canJump = false;
    boolean jumping = false;
    int highestJump = 100;
    boolean falling = true;
    public Character(int x, int y, KeyCode leftKey, KeyCode rightKey, KeyCode upKey) {
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/platformer/assets/StillMario.png"));
        this.imageView = new ImageView(characterImg);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.getChildren().addAll(this.imageView);
    }
    public void moveY() {
        if(falling) {
            y = y + yVelocity;
        }
        else if(jumping) {
            y = y - yVelocity;
        }
    }
    public void jump() {
        if (canJump) {
            yVelocity = 3;
            canJump = false;
            jumping = true;
            falling = false;
        }
    }
    public void checkReachHighest () {
        if(jumping && highestJump > y) {
            jumping = false;
            falling = true;
        }
    }
    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
            falling = false;
            canJump = true;
        }
    }
    public void repaint() {
        setTranslateX(x);
        setTranslateY(y);
    }
    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-(int)getWidth();
        }
    }
    public void moveLeft() {
        x = x-xVelocity;
    }
    public void moveRight() {
        x = x+xVelocity;
    }
    public void stop() {
        xVelocity = 5;
    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }
}
