package platformer;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import platformer.controller.GameLoop;
import platformer.view.Platform;

public class Main extends Application {
    public static void main(String[] args) { launch(args); }
    @Override
    public void start(Stage primaryStage) {
        Platform platform = new Platform();
        GameLoop gameLoop = new GameLoop(platform);
        Scene scene = new Scene(platform,Platform.WIDTH,Platform.HEIGHT);
        scene.setOnKeyPressed(event-> platform.getKeys().add(event.getCode()));
        scene.setOnKeyReleased(event -> platform.getKeys().remove(event.getCode()));
        primaryStage.setTitle("platformer");
        primaryStage.setScene(scene);
        primaryStage.show();
        (new Thread(gameLoop)).start();
    }
}

