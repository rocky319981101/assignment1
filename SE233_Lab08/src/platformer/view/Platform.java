package platformer.view;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import platformer.model.Character;
import platformer.model.Keys;

public class Platform extends Pane {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public final static int GROUND = 300;
    private Image platformImg;
    private Character character;

    private Keys keys;
    public Platform() {
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/platformer/assets/Background.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        character = new Character(30, 30, KeyCode.A,KeyCode.D,KeyCode.W);

        getChildren().addAll(backgroundImg,character);
    }


    public Keys getKeys() {
        return keys;
    }



    public Character getCharacter() {
        return character;
    }


}